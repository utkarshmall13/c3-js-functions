
function draw_linechart(data , elementid ){
	var chart = c3.generate({
		bindto: '#'+elementid,
		data: {
		columns: data
		}
		});
}

function draw_piechart(data , elementid){
	var chart = c3.generate({
    bindto: '#'+elementid,
    data: {
        // iris data from R
        columns: data,
        type : 'pie',
    }
	});
}
